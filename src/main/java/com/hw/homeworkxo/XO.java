/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.homeworkxo;

/**
 *
 * @author BenZ
 */
import java.util.*;
public class XO {
    public static void printtable(char [][] table){
        System.out.println("  1 2 3");
        for(int i=0;i<table.length;i++ ){
            System.out.print(i+1+" ");
            for(int j=0 ;j<table[i].length ;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    //table == {{'X','X','X' },{'O','O','-' },{'-','-','-' }};
    public static char checkWinner(char [][] table,char xo){//table ==
        char win = '-'; 
        int row = 0;
	int col = 0;
        int cross1 = 0;
        int cross2 = 0;
        while(win == '-') {
            for(int i=0;i<table.length;i++) {
                row = 0;
                col = 0;
                cross1 =0;
                cross2 =0;
		for(int j=0 ;j<table[i].length ;j++) {
			if(table[i][j] == xo)
                            row++;
                        if(table[j][i] == xo)
                            col++;
		}
                if(table[0][0] == xo && table[1][1] == xo &&table[2][2] == xo)
                    cross1 = 3;//L to R
                
                if(table[0][2] == xo && table[1][1] == xo &&table[2][0] == xo)
                    cross2 = 3;// R to L
                
                if(i==table.length-1&& win == '-')
                    win = 's';
                
		if(row == 3 || col == 3 || cross1 == 3 || cross2 == 3) 
                    win = xo;
            }
            
            
        }
        return win;    
    }
    public static void main(String[] args) {
        int col,row;
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
       
        char [][] table = new char[][]{{'-','-','-' },{'-','-','-' },{'-','-','-' }};
        printtable(table);
        
        boolean stop = true;
        char xo = 'X';
        int due =0;
        do{
            System.out.println(xo+" turn");
            System.out.println("Please input Row Col :");
            row = kb.nextInt();
            col = kb.nextInt();
            table[row-1][col-1] = xo;
            due++;
            printtable(table);
            char winner = checkWinner(table,xo);
            if(winner != 's' ){
                System.out.println("Player "+winner+" winner");
                stop = false;
            }else if(due == 9){
                System.out.println("No winner");
            }
            
                
            if(xo == 'X')
                xo = 'O';
            else
                xo = 'X';
            
        }while(stop);
        
        System.out.println("Good Game");
        
        
    }
    
}
